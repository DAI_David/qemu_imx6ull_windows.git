# 搭建IMX6ULL环境(基于百问网zImage和rootfs.img)
## 1. 软件环境准备
[QEMU_Windows环境下载](https://gitee.com/DAI_David/qemu_imx6ull_windows/repository/archive/master.zip)

[ConEmu终端官网](https://conemu.github.io/)

[ConEmu终端百度云链接](https://pan.baidu.com/s/1Akq9dOZWiu4dCAzHBXFNdQ)提取码:7vib

## 2.环境部署
### 2.1 运行qemu
    > 解压DAI_David-qemu_imx6ull_windows-master.zip到本地电脑
    > 进入到DAI_David-qemu_imx6ull_windows-master\qemu_imx6ull_windows
    > 双击运行QEMU_WIN.bat根据需求选择对应模式[gui|nogui]
    > 输入用户密码：
    > user: root
    > passwd: 123456
    > 终端出现乱码如下：
![image](bmp/messy_code.png)

### 2.2 ComEmu终端运行qemu
    > 这时候ComEmu终端就派上用场了
    > 解压ConEmuPack.191012.7z
    > 进入到ConEmuPack.191012目录
    > 双击运行ConEmu64.exe(可以更改语言后点击ok)
    > cd进入你的qemu解压路径DAI_David-qemu_imx6ull_windows-master\qemu_imx6ull_windows
    > $ QEMU_WIN.bat
    > 设置ComEmu(Win+alt+p)替代系统cmd命令后双击QEMU_WIN.bat,设置如下：
![image](bmp/ComEmu_set.png)

    > 启动完成后就就不会想刚才那样乱码了
    > 警告:
    1. 该终端不能键入ctrl+c去结束QEMU里面运行的进程，需要键入ctrl+shift+c
    2. 该终端不能使用键盘的方向键
    > 故解决方法可以使用ssh工具例如：putty/xshell/MobaXterm等工具连接QEMU_IMX6ULL
    > ssh配置如下:
    ip:127.0.0.1
    user: root
    passwd: 123456
    > 连接完成后就无上述警告限制了，至此全部环境搭建完成
![image](bmp/ssh.png)
