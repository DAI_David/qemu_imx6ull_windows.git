@echo off
set ROOT_DIR=%~dp0

set QEMU_TOOLS=%ROOT_DIR%\qemu\bin\qemu-system-arm.exe
set ZIMAGE_FILE=%ROOT_DIR%\imx6ull-system-image\zImage
set DTB_FILE=%ROOT_DIR%\imx6ull-system-image\100ask_imx6ull_qemu.dtb
set ROOTFS_FILE=%ROOT_DIR%\imx6ull-system-image\rootfs.img

rem parameter setting
set MACHINES=mcimx6ul-evk
set MEMORY_SIZE=512M
set NO_DISPLAY=-nographic
set DISPLAY=-display sdl -show-cursor
set SERIAL_PORT=mon:stdio
set DRIVE=format=raw,id=mysdcard
set DEVICE=sd-card,drive=mysdcard
set APPENV="console=ttymxc0,115200 rootfstype=ext4 root=/dev/mmcblk1  rw rootwait init=/sbin/init  loglevel=8"
set NETWORK=-nic user,hostfwd=tcp::22-:22
set BOARD_TYPE[1]=100ask
set BOARD_TYPE[2]=fire
set BOARD_TYPE[3]=atk
set DISPLAY_TYPE=%1
set BOARD=%2

rem %QEMU_TOOLS% -M mcimx6ul-evk -m 512m -kernel %ZIMAGE_FILE% -dtb %DTB_FILE% %NO_DISPLAY% -serial mon:stdio -drive  file=%ROOTFS_FILE%,format=raw,id=mysdcard -device sd-card,drive=mysdcard -append "console=ttymxc0,115200 rootfstype=ext4 root=/dev/mmcblk1  rw rootwait init=/sbin/init  loglevel=8" -net nic -net tap,ifname=tap 

rem %QEMU_TOOLS% -M %MACHINES% -m %MEMORY_SIZE% -kernel %ZIMAGE_FILE% -dtb %DTB_FILE% %DISPLAY% -serial %SERIAL_PORT% -drive file=%ROOTFS_FILE%,%DRIVE% -device %DEVICE% -append %APPENV% %NETWORK% -com 100ask

if "%DISPLAY_TYPE%"=="" (
	goto _help
	) else (
	goto _start
	)

:_help
set var=""
echo "help(%0 [gui|nogui] [100ask|fire|atk]):"
echo "	1. gui"
echo "	2. nogui "
set /p var=Please input manually: 
if "%var%"=="1" (
:gui_type
	set num=""
	echo "board_type:"
	echo "	1. 100ask"
	echo "	2. fire "
	echo "	3. atk "
	set /p num=Please input manually: 
	goto gui_type_get
	) else (
		if "%var%"=="2" (
			goto nogui_start
		) else (
			echo "Input error!"
			goto _help
		)
	)
:_start
if "%DISPLAY_TYPE%"=="gui" (
	goto gui_start
	) else (
	if "%DISPLAY_TYPE%"=="nogui" (
		goto nogui_start
		) else (
		goto _help
		)
	)

:gui_type_get
for /f "delims=" %%i in ('SET BOARD_TYPE[%num%]') do (set BOARD_BAK=%%i)
set BOARD=%BOARD_BAK:~14%
goto gui_start

:gui_start
if "%BOARD%"=="" (
	echo "Input error!"
	goto gui_type
	)
echo "gui:%BOARD%, %BOARD_BAK%"
%QEMU_TOOLS% -M %MACHINES% -m %MEMORY_SIZE% -kernel %ZIMAGE_FILE% -dtb %DTB_FILE% %DISPLAY% -serial %SERIAL_PORT% -drive file=%ROOTFS_FILE%,%DRIVE% -device %DEVICE% -append %APPENV% %NETWORK% -com %BOARD%
goto end

:nogui_start
%QEMU_TOOLS% -M %MACHINES% -m %MEMORY_SIZE% -kernel %ZIMAGE_FILE% -dtb %DTB_FILE% %NO_DISPLAY% -serial %SERIAL_PORT% -drive file=%ROOTFS_FILE%,%DRIVE% -device %DEVICE% -append %APPENV% %NETWORK%
goto end

:end
pause